﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Squire.UserControls {
	/** Stackpanel which divides space evenly between children **/
	class SharingStackPanel : StackPanel {
		public SharingStackPanel() {
			Orientation = Orientation.Horizontal;
		}
		protected override Size MeasureOverride(Size constraint) {
			if (Orientation == Orientation.Vertical) {
				throw new NotImplementedException("Vertical orientation not implemented");
			}
			var biggestWidth = 0.0;
			var biggestHeight = 0.0;

			var constraintPerChild = new Size(constraint.Width / InternalChildren.Count, constraint.Height);

			foreach (UIElement child in InternalChildren) {
				child.Measure(constraintPerChild);
				if (child.DesiredSize.Width > biggestWidth) {
					biggestWidth = child.DesiredSize.Width;
				}
				if (child.DesiredSize.Height > biggestHeight) {
					biggestHeight = child.DesiredSize.Height;
				}
			}

			return new Size(biggestWidth * InternalChildren.Count, biggestHeight);
		}
		protected override Size ArrangeOverride(Size arrangeBounds) {

			if (Orientation == Orientation.Vertical) {
				throw new NotImplementedException("Vertical orientation not implemented");
			}
			var usedHorizontal = 0.0;
			var maxVertical = 0.0;
			foreach (UIElement child in InternalChildren) {
				child.Arrange(new Rect(usedHorizontal, 0, arrangeBounds.Width / InternalChildren.Count, arrangeBounds.Height));
				usedHorizontal += arrangeBounds.Width / InternalChildren.Count;
				maxVertical = Enumerable.Max<double>(new double[] { maxVertical, child.DesiredSize.Height });
			}
			return new Size(usedHorizontal, maxVertical);
		}
	}
}
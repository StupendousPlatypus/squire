﻿using Squire.ViewModel;
using System.Diagnostics;
using System.Windows;

namespace Squire {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
		}

        private async void Window_Loaded(object sender, RoutedEventArgs e) {
			var squireVm = DataContext as SquireViewModel;
			Debug.Assert(squireVm != null);
			await squireVm.LoadCards();
        }

	}
}

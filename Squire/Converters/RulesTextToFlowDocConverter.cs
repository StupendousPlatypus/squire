﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Squire.Converters {
	public class RulesTextToFlowDocConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null) {
				return null;
			} else if (value is IEnumerable<IEnumerable<object>>) {
				var doc = new FlowDocument();
				doc.Blocks.AddRange(from p in (value as IEnumerable<IEnumerable<object>>)
									select ListToPara(p));
				if (parameter is FontFamily) {
					doc.FontFamily = (FontFamily)parameter;
				}
				return doc;
			}
			throw new NotImplementedException("Can't convert to " + targetType);
		}
		private Paragraph ListToPara(IEnumerable<object> list) {
			var ret = new Paragraph();
			foreach (var o in list) {
				if (o is BitmapImage) {
					var glyphImage = new Image {
                        Stretch = Stretch.None,
                        Source = (BitmapImage) o
                    };
				    ret.Inlines.Add(glyphImage);
				} else {
					ret.Inlines.Add(o.ToString());
				}
			}
			return ret;
		}
		public object ConvertBack(object value, Type targetType, object paramter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}

﻿using System.Collections.Generic;
using Squire.Model.Magic;
using Squire.Model.Storage;

namespace Squire.ViewModel {
	public class MagicCardVm {
	    readonly MagicCard _card;
		public MagicCard InnerCard { get { return _card; } }
		public List<MagicFaceVm> Faces { get; set; }
		public MagicCardVm(MagicCard card, FaceImageStorage imageStorage, GlyphStorage glyphStorage) {
			Faces = new List<MagicFaceVm>();
			_card = card;
			foreach (var face in card.Faces) {
				Faces.Add(new MagicFaceVm(face, imageStorage, glyphStorage));
			}
		}
		public int SetNumber { get { return _card.SetNumber; } }
		public string MagicSet { get { return _card.MagicSet; } }
		public string Rarity { get { return _card.Rarity; } }
		public MagicCard.ECompoundType CompoundType { get { return _card.CompoundType; } }

	}
}

﻿using System;
using Squire.Model.Filters;
using Squire.Model.Storage;

namespace Squire.ViewModel.SquireFilter {
	public class FilterVm {
		public event EventHandler UpdateFilterEvent = delegate { };
		NameFilter NameFilter { get; set; }
		public String NameFilterText {
			get { return NameFilter.SearchedString; }
			set {
				NameFilter.SearchedString = value;
				UpdateFilterEvent(this, EventArgs.Empty);
			}
		}

	    readonly ColorFilter _colorFilter;
		public ColorFilterVm ColorFilterVm {
			get;
			private set;
		}
	
		public FilterVm(GlyphStorage glyphSource) {
			_colorFilter = new ColorFilter();
			ColorFilterVm = new ColorFilterVm(_colorFilter, glyphSource);
			foreach(var c in _colorFilter.ColorChoices){
				c.SelectionChangedEvent += RaiseUpdateFilterEvent;
			}
			NameFilter = new NameFilter();
		}
		public bool FilterDel(object cardObj) {
			var card = cardObj as MagicCardVm;
			if (card == null) {
				return false;
			}
			return
				NameFilter.FilterImpl(card.InnerCard)
				&& _colorFilter.FilterImpl(card.InnerCard);
		}
		public void RaiseUpdateFilterEvent(object sender, EventArgs e) {
			UpdateFilterEvent(this, e);
		}
	}
}

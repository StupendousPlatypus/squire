﻿using System.Collections.Generic;
using System.Linq;
using Squire.Model.Filters;
using Squire.Model.Storage;
using Squire.Model.Utilities;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace Squire.ViewModel.SquireFilter {
	public class ColorFilterVm {
		private ColorFilter _filter;
		public List<ColorChoiceVm> Choices {
			get;
			private set;
		}
		public ColorFilterVm(ColorFilter filter, GlyphStorage glyphSource) {
			_filter = filter;
			var cs = from c in filter.ColorChoices
					 select new ColorChoiceVm(c, glyphSource);
			Choices = cs.ToList();
		}
		public class ColorChoiceVm : INotifyPropertyChanged {
			public event PropertyChangedEventHandler PropertyChanged = delegate { };
			private readonly ColorFilter.ColorChoice _choice;
			private readonly GlyphStorage _glyphSource;
			bool _glyphAvailable;
			public bool GlyphAvailable {
				get {
					return _glyphAvailable;
				}
				private set {
					_glyphAvailable = value;
					PropertyChanged(this, new PropertyChangedEventArgs("GlyphAvailable"));
				}
			}
			public bool IsSelected {
				get { return _choice.IsSelected; }
				set { _choice.IsSelected = value; }
			}
			public BitmapImage GlyphImage {
				get {
					if (!GlyphAvailable) {
						return null;
					}
					return _glyphSource.GetGlyphBitmap('l'+_choice.Name);
				}
			}
			public string Name {
				get { return _choice.Name; }
			}
			public ColorChoiceVm(ColorFilter.ColorChoice choice, GlyphStorage glyphSource) {
				GlyphAvailable = false;
				_glyphSource = glyphSource;
				_choice = choice;
				glyphSource.SubscribeResourceAvailable('l'+choice.Name, OnGlyphAvailable);
			}
			private void OnGlyphAvailable(object sender, NotifyResourceAvailableArgs e){
				GlyphAvailable = true;
				PropertyChanged(this, new PropertyChangedEventArgs("GlyphImage"));
				
			}
		}
	}
}

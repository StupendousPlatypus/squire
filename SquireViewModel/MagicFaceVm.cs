﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using Squire.Model.Magic;
using Squire.Model.Storage;

namespace Squire.ViewModel {
	public class MagicFaceVm {
		protected MagicFace Face;
		private readonly FaceImageStorage _faceStorage;
		private readonly GlyphStorage _glyphStorage;
		public MagicFaceVm(MagicFace face, FaceImageStorage imageStorage, GlyphStorage glyphStorage) {
			Face = face;
			_faceStorage = imageStorage;
			_glyphStorage = glyphStorage;
		}
		public string Cost { get { return Face.Cost; } }
		public HashSet<string> MagicSupertypes { get { return Face.MagicSupertypes; } }
		public HashSet<string> MagicSubtypes { get { return Face.MagicSubtypes; } }
		public HashSet<string> MagicTypes { get { return Face.MagicTypes; } }
		public string MagicTypesLine { get { return Face.MagicTypesLine; } }
		public List<string> Text { get { return Face.Text; } }
		public string Flavor { get { return Face.Flavor; } }
		public string Name { get { return Face.Name; } }
		public MagicCard Parent;
		public bool HasStats { get { return Face.Power != null && Face.Toughness != null; } }
		public bool HasLoyalty { get { return Face.Loyalty.HasValue; } }
		public int Loyalty { get { return 
			Face.Loyalty.HasValue ? (int) Face.Loyalty : 0; 
		} }

		public Uri FaceImage { 
			get { 
				return _faceStorage.GetFaceImageAsync(Face).Result;
			} 
		}

		public IEnumerable<BitmapImage> CostBitmaps {
			get {
				var parsing = Cost;
				var ret = new List<BitmapImage>();
				while (parsing != null && parsing.IndexOf('[') > -1) {
					var glyphName = parsing.Substring(parsing.IndexOf('[') + 1);
					glyphName = glyphName.Remove(glyphName.IndexOf(']'));
					var glyphKey = 'l' + glyphName;
					var bitmap = _glyphStorage.GetGlyphBitmap(glyphKey);
					ret.Add(bitmap);
					parsing = parsing.Substring(parsing.IndexOf(']') + 1);
				}
				return ret;
			}
		}

		public IEnumerable<IEnumerable<object>> RulesTextParagraphs {
			get {
				return from txt in Text
					   select ConvertStringToPara(txt);
			}
		}
		private List<object> ConvertStringToPara(string txt) {
			var parsing = txt;
			var ret = new List<object>();
			while (parsing.IndexOf('[') > -1) {
				ret.Add(parsing.Substring(0, parsing.IndexOf('[')));

				var glyphName = parsing.Substring(parsing.IndexOf('[') + 1);
				glyphName = glyphName.Remove(glyphName.IndexOf(']'));
				ret.Add(_glyphStorage.GetGlyphBitmap('s' + glyphName));
				parsing = parsing.Substring(parsing.IndexOf(']') + 1);
			}

			ret.Add(parsing);
			return ret;
		}

		public string Stats {
			get {
				if (!(Face.Power.HasValue && Face.PowerStar.HasValue &&
					Face.Toughness.HasValue && Face.ToughnessStar.HasValue)) {
					return "";
				}
				string powString;
				if (Face.PowerStar.Value && Face.Power > 0) {
					powString = Face.Power + "+" + "*";
				} else if (Face.PowerStar.Value) {
					powString = "*";
				} else {
					powString = Face.Power.ToString();
				}
				string toughString;
				if (Face.ToughnessStar.Value && Face.Toughness > 0) {
					toughString = Face.Toughness + "+" + "*";
				} else if (Face.ToughnessStar.Value) {
					toughString = "*";
				} else {
					toughString = Face.Toughness.ToString();
				}
				return powString + " / " + toughString;
			}
		}
	}
}

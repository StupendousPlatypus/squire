﻿using System;
using System.Threading.Tasks;
using Squire.Model;
using System.ComponentModel;
namespace Squire.ViewModel
{
    public class DownloaderVm : INotifyPropertyChanged
    {
		public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public ExpansionsDownloader Downloader { get; set; }
        public string CurrentTask { get { return Downloader.CurrentTask; } }
        public double PercentProgress { get { return Downloader.PercentProgress; } }
		public bool IsActive { get { return Downloader.IsActive; } }

        public DownloaderVm(ExpansionsDownloader downloader) {
            Downloader = downloader;
            Downloader.PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(Object sender, PropertyChangedEventArgs args) {
			PropertyChanged(this, args);
		}
    }
}

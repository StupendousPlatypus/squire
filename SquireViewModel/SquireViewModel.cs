﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Data;

using Squire.Model;
using Squire.Model.Magic;
using Squire.ViewModel.SquireFilter;

namespace Squire.ViewModel {
	public class SquireViewModel : INotifyPropertyChanged {
		public event PropertyChangedEventHandler PropertyChanged = delegate {};

	    readonly SquireModel _model = new SquireModel();
		public ObservableCollection<MagicCardVm> ViewableCards {get; private set;}
		public ICollectionView CardsView { get; set; }
		public FilterVm SquireFilter { get; set; }
		private MagicCardVm _selectedCard;
		public MagicCardVm SelectedCard { 
			get { return _selectedCard; }
			set { _selectedCard = value; PropertyChanged(this, new PropertyChangedEventArgs("SelectedCard")); }
		}
        public DownloaderVm DownloadVm { get; set; }
		public SquireViewModel() {
			ViewableCards = new ObservableCollection<MagicCardVm>();
			CardsView = CollectionViewSource.GetDefaultView(ViewableCards);
			(CardsView as ListCollectionView).CustomSort = new MagicCardSortComparer();
			_model.CardList.CollectionChanged += CardList_CollectionChanged;
            DownloadVm = new DownloaderVm(_model.Downloader);
			SquireFilter = new FilterVm(_model.GlyphStorage);
			CardsView.Filter = SquireFilter.FilterDel;
			SquireFilter.UpdateFilterEvent += SquireFilter_UpdateFilterEvent;
		}

		public async Task LoadCards() {
			await _model.LoadCards();
		}

		void SquireFilter_UpdateFilterEvent(object sender, EventArgs e) {
			CardsView.Refresh();
		}
		void CardList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e) {
			foreach (MagicCard addedCard in e.NewItems) {
				ViewableCards.Add(new MagicCardVm(addedCard, _model.ImageStorage, _model.GlyphStorage));
			}
		}
	}
}
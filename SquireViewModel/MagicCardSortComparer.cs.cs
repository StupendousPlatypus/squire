﻿using System;
using System.Collections;

namespace Squire.ViewModel {
	class MagicCardSortComparer : IComparer{
		public int Compare(Object cardObj1, Object cardObj2) {
			var card1 = (MagicCardVm)cardObj1;
			var card2 = (MagicCardVm)cardObj2;
			if (card1.MagicSet.CompareTo(card2.MagicSet) != 0) {
				return card1.MagicSet.CompareTo(card2.MagicSet);
			} else {
				return card1.SetNumber - card2.SetNumber;
			}
		}
	}
}


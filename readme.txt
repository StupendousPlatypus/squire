This is a simple application which displays cards from the game 'Magic: The Gathering'. It scrapes them from Magic's official database, Gatherer. Since this program is so simple, you are better off using Gatherer if you are actually interested in browsing cards.

This application is developed on WPF 4.5 targeting .NET 4.5. Therefore, it only runs on Windows Vista and later.

Downloaded resources are stored in "%ProgramData%/Squire/". Since there is not yet an uninstaller, don't forget to delete this folder when you're done.
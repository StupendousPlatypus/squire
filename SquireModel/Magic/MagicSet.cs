﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Squire.Model.Magic {
	[XmlRoot("BlockCollection")]
	public class BlockCollection {
		[XmlElement("Block")]
		public Block[] Blocks { get; set; }
	}
	[Serializable]
	public class Block {
		[XmlAttribute]
		public String Name { get; set; }
		[XmlElement("Set")]
		public MagicSet[] Sets { get; set; }
	}
	[Serializable]
	public class MagicSet {
		[XmlAttribute]
		public string Name { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Squire.Model.Magic {
	[Serializable]
	public class MagicCard {
		public static string[] CardTypes = new string[] { "Land", "Creature", "Instant", "Sorcery", "Artifact", "Enchantment", "Planeswalker" };
		public static string[] SuperTypes = new string[] { "Basic", "Legendary", "Tribal" };


		[DataMember]
		public int SetNumber { get; set; }

		[DataMember]
		public int MultiverseId { get; set; }

		[DataMember]
		public List<MagicFace> Faces { get; set; }

		[DataMember]
		public string MagicSet { get; set; }
	    public MagicSet MSet { get; set; }

		[DataMember]
		public string Rarity { get; set; }

		public enum ECompoundType { Simple, Split, Splitfuse, Flip, Transform }
		[DataMember]
		public ECompoundType CompoundType;

		public string Cost { get { return Faces[0].Cost; } }
		public ISet<string> MagicSupertypes { get { return (ISet<string>)Faces.SelectMany(a => a.MagicSupertypes); } }
		public ISet<string> MagicTypes { get { return (ISet<string>)Faces.SelectMany(a => a.MagicTypes); } }
		public ISet<string> MagicSubtypes { get { return (ISet<string>)Faces.SelectMany(a => a.MagicSubtypes); } }

		public string Name {
			get {
				if (Faces.Count == 2) {
					return Faces[0].Name + "//" + Faces[1].Name;
				}
				return Faces[0].Name;
			}
		}

		public string MagicTypesLine {
			get {
				var retval = Faces.SelectMany(a => a.MagicSupertypes).Aggregate("", (current, s) => current + (s + " "));
			    retval += Faces.SelectMany(a => a.MagicTypes).Aggregate(retval, (current, s) => current + (s + " "));
			    var subtypes = Faces.SelectMany(a => a.MagicSubtypes);
				if (subtypes.Any())
				{
				    retval += "– ";
				    retval = subtypes.Aggregate(retval, (current, s) => current + (s + " "));
				}
			    retval = retval.TrimEnd();
				return retval;
			}
		}

		public MagicCard() {
		}
		[OnDeserialized]
		private void OnDeserializedMethod(StreamingContext dontcare) {
			Faces.ForEach(face => face.Parent = this);
		}
	}
}
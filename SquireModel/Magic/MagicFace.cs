﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Squire.Model.Magic {
	[Serializable]
	public class MagicFace {
		[DataMember]
		public string FaceId;
		[DataMember]
		public string Cost { get; set; }
		[DataMember]
		public HashSet<string> MagicSupertypes = new HashSet<string>();
		[DataMember]
		public HashSet<string> MagicTypes = new HashSet<string>();
		[DataMember]
		public HashSet<string> MagicSubtypes = new HashSet<string>();
		[DataMember]
		public List<string> Text { get; set; }
		[DataMember]
		public string Flavor { get; set; }
		[DataMember]
		public string Name { get; set; }
		[DataMember]
		public Uri ImageOnWeb { get; set; }
		[DataMember]
		public int? Power { get; set; }
		[DataMember]
		public bool? PowerStar { get; set; }
		[DataMember]
		public int? Toughness { get; set; }
		[DataMember]
		public bool? ToughnessStar { get; set; }
		[DataMember]
		public int? Loyalty { get; set; }

		[NonSerialized]
		public MagicCard Parent;

		public MagicFace() {
			Text = new List<string>();
		}

		public string MagicTypesLine {
			get {
				var retval = "";
				foreach (var s in MagicSupertypes) {
					retval += s + " ";
				}
				foreach (var s in MagicTypes) {
					retval += s + " ";
				}
				if (MagicSubtypes.Count > 0) {
					retval += "– ";
					foreach (var s in MagicSubtypes) {
						retval += s + " ";
					}
				}
				retval.TrimEnd();
				return retval;
			}
		}
	}
}

﻿using System;

namespace Squire.Model.Utilities {
	interface INotifiesResourceAvailable {
		/**
		 * If the resource is available now, immediately calls the handler.
		 * Otherwise, calls the handler when it becomes available.
		 * */
		void SubscribeResourceAvailable(string desiredResource, NotifyResourceAvailableHandler handler);
	}
	public delegate void NotifyResourceAvailableHandler(object sender, NotifyResourceAvailableArgs e);
	public class NotifyResourceAvailableArgs : EventArgs {
		public string Resource { get; set; }
	}
}
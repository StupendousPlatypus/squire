﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

using Squire.Model.Utilities;
using System.Diagnostics;

namespace Squire.Model.Storage {
	public class GlyphStorage : LocalStorage, INotifiesResourceAvailable {
		private readonly Dictionary<string, NotifyResourceAvailableHandler> _resourceSubscribers
			= new Dictionary<string, NotifyResourceAvailableHandler>();
		private static readonly string Glyphpath = Path.Combine(Graphicspath, "Glyphs");
		private readonly Dictionary<string, BitmapImage> _glyphsCache = new Dictionary<string, BitmapImage>();
		public GlyphStorage (WebClientPool pool) : base(pool){
		}

		public void SubscribeResourceAvailable(
			string resource,
			NotifyResourceAvailableHandler handler) {

			if (_glyphsCache.ContainsKey(resource)) {
				Trace.TraceInformation("Already cached. Immediately notifying subscriber for" + resource);
				handler(this, new NotifyResourceAvailableArgs { Resource = resource });
				return;
			}

            var filepath = GetGlyphSource(resource);
            var fileAlreadyExists = File.Exists(filepath.LocalPath); //might be being written still
            if (fileAlreadyExists) {
                var glyphReady = false;
                lock (LocalFilesWritingLock) {
                    if (!LocalFilesWriting.ContainsKey(resource)) {
                        glyphReady = true;
                    }
                }
                if (glyphReady) {
                    Trace.TraceInformation("Already on disk, not yet cached. Immediately notifying subscriber for" + resource);
                    CacheSymbolBitmap(resource);
                    handler(this, new NotifyResourceAvailableArgs { Resource = resource });
                    return;
                }
            }

			Trace.TraceInformation("Unavailable resource. Adding subscriber for" + resource);
			if (!_resourceSubscribers.ContainsKey(resource)) {
				_resourceSubscribers[resource] =
					new NotifyResourceAvailableHandler(
						(object sender, NotifyResourceAvailableArgs e) => { });
			}
			_resourceSubscribers[resource] += handler;
		}

		public async Task StoreGlyphAsync(string glyphName, Uri uri) {
			glyphName = glyphName.Replace(":", "");
			
			var filepath = Path.Combine(Glyphpath, glyphName + ".jpeg");

			if (!Directory.Exists(Glyphpath)) {
				Directory.CreateDirectory(Glyphpath);
			}

			if (_glyphsCache.ContainsKey(glyphName)) {
				return;
			}

			await StoreAsync(filepath, uri);

			if (!_glyphsCache.ContainsKey(glyphName)) {
				CacheSymbolBitmap(glyphName);
			}
		}

		public Uri GetGlyphSource(string glyphName) {
			return new Uri(Path.Combine(Glyphpath, glyphName.Replace(":", "") + ".jpeg"));
		}
		
		public BitmapImage GetGlyphBitmap(string glyphName) {
			if (!_glyphsCache.ContainsKey(glyphName)) {
				CacheSymbolBitmap(glyphName);
			}
			return _glyphsCache[glyphName];	
		}

		private void CacheSymbolBitmap(string glyphName) {
			var bmp = new BitmapImage(GetGlyphSource(glyphName));
			_glyphsCache[glyphName] = bmp;

			if (_resourceSubscribers.ContainsKey(glyphName)) {
				Trace.TraceInformation("Resource became available: " + glyphName);
				_resourceSubscribers[glyphName](this, new NotifyResourceAvailableArgs { Resource = glyphName });
				_resourceSubscribers.Remove(glyphName);
			}
		}
	}
}

﻿using Squire.Model.Magic;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Squire.Model.Storage {
	public class FaceImageStorage : SerializedCardStorage {
		private static readonly string FaceGraphicsPath = Path.Combine(Graphicspath, "Cards");
		public FaceImageStorage(WebClientPool pool)
			: base(pool) {
		}
		private string ImageDirectoryPath(MagicFace face) {
			return Path.Combine(FaceGraphicsPath, face.Parent.MagicSet.Replace(":", ""));
		}
		private string ImageFilePath(MagicFace face) {
			var targetDirectory = ImageDirectoryPath(face);
			string filename;
			if (face.Parent.CompoundType == MagicCard.ECompoundType.Transform ||
				face.Parent.CompoundType == MagicCard.ECompoundType.Flip) {
				char extraLetter;
				if (face == face.Parent.Faces[0]) {
					extraLetter = 'L';
				} else {
					extraLetter = 'R';
				}
				filename = face.Parent.SetNumber.ToString() + extraLetter + ".jpeg";
			} else {
				filename = face.Parent.SetNumber + ".jpeg";
			}
			return Path.Combine(targetDirectory, filename);
		}
		/**
		 * Returns a Uri to a locally stored image for the face. If an image is
		 * not available locally, will asynchronously download one and return
		 * after it is finished.
		 */
		public async Task<Uri> GetFaceImageAsync(MagicFace face) {
			var targetDirectory = ImageDirectoryPath(face);
			if (!Directory.Exists(targetDirectory)) {
				Directory.CreateDirectory(targetDirectory);
			}

			await StoreAsync(ImageFilePath(face), face.ImageOnWeb);

			return new Uri(ImageFilePath(face));
		}
	}
}

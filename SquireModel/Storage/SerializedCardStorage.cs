﻿using System.Xml.Serialization;
using Squire.Model.Magic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Squire.Model.Storage {
	public class SerializedCardStorage : LocalStorage {

		public SerializedCardStorage(WebClientPool pool)
			: base(pool) {
		}
		public void StoreCard(MagicCard card) {
			string fullPathThisCard = null;
			lock (Cardspath) {
				if (!Directory.Exists(Cardspath)) {
					Directory.CreateDirectory(Cardspath);
				}
				var dirThisCard = Path.Combine(Cardspath, card.MagicSet.Replace(":", ""));
				fullPathThisCard = Path.Combine(dirThisCard, card.SetNumber + ".xml");
				if (!Directory.Exists(dirThisCard)) {
					Directory.CreateDirectory(dirThisCard);
				}
				if (File.Exists(fullPathThisCard)) {
					File.Delete(fullPathThisCard);
				}
			}
			using (var fileStream = File.Create(fullPathThisCard)) {
				var serializer = new DataContractSerializer(card.GetType());
				serializer.WriteObject(fileStream, card);
			}
		}
		public void NoteSetComplete(MagicSet set) {
            var setpath = Path.Combine(Cardspath, set.Name.Replace(":", ""));
			if (!Directory.Exists(setpath)) {
				throw new Exception("Called 'NoteSetComplete' on non-existent set");
			}
			File.Create(Path.Combine(setpath, ".complete")).Close();
		}
		public bool IsSetComplete(MagicSet set) {
			var setpath = Path.Combine(Cardspath, set.Name);
			if (!Directory.Exists(setpath)) {
				return false;
			}
			var dirInfo = new DirectoryInfo(setpath);
			return dirInfo.GetFiles().Any(file => file.Name == ".complete");
		}
		public async Task GetCardsFromFileSystem(IEnumerable<MagicSet> sets, Collection<MagicCard> cards) {
			var dirInfo = new DirectoryInfo(Cardspath);
			if (!dirInfo.Exists) {
				return;
			}

            //select directories with a corresponding set
		    var setDirs = dirInfo.GetDirectories();
		    foreach (var set in sets) {
		        var setDir = (from sd in setDirs
		            where (sd.Name == set.Name.Replace(":", ""))
		            select sd).SingleOrDefault();
                if(setDir==null) {
                    //skip sets where no corresponding directory exists yet
		            continue;
		        }
                var magicSetTask = new Task<Tuple<List<MagicCard>, bool>>(() => {
                    var readError = false;
                    var singleSetCards = new List<MagicCard>();
                    var fileInfos = setDir.GetFiles("*.xml");
                    var serializer = new DataContractSerializer(typeof(MagicCard));
                    foreach (var fileInfo in fileInfos) {
                        try {
                            using (var reader = fileInfo.OpenRead()) {
                                var card = (MagicCard) serializer.ReadObject(reader);
                                card.MSet = set;
                                singleSetCards.Add(card);
                                reader.Close();
                            }
                        } catch (SerializationException e) {
                            Trace.TraceWarning("Error reading " + fileInfo.ToString());
                            Trace.TraceWarning(e.ToString());
                            readError = true;
                        }
                    }
                    return new Tuple<List<MagicCard>, bool>(singleSetCards, readError);
                });
                magicSetTask.Start();
                var magicSetResult = await magicSetTask;
                foreach (var card in magicSetResult.Item1) {
                    cards.Add(card);
                }
                if (magicSetResult.Item2) {
                    //had an error reading the card, delete ".complete" so it can be downloaded again.
                    var completionNote = new FileInfo(Path.Combine(setDir.FullName, ".complete"));
                    if (completionNote.Exists) {
                        File.Delete(completionNote.ToString());
                    }
                }
		    }
			return;
		}
	}
}

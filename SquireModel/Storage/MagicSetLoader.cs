﻿using System;
using System.IO;
using System.Xml.Serialization;
using Squire.Model.Magic;
using System.Collections.Generic;
using System.Linq;

namespace Squire.Model.Storage {
    class MagicSetLoader {
        public IEnumerable<MagicSet> LoadSetNames() {
            var reader = new XmlSerializer(typeof(BlockCollection));
            Object o;
            using (var stream = GetType().Assembly.GetManifestResourceStream("Squire.Model.Resources.Blocks.xml")) {
                if (stream == null) {
                    throw new Exception("Failed to get resource tream: Squire.Model.Resources.Blocks.xml");
                }
                using (var file = new StreamReader(stream)) {
                    o = reader.Deserialize(file);
                }
            }
			var blockCollection = (BlockCollection) o;
			return blockCollection.Blocks.SelectMany(block => block.Sets);
        }
    }
}

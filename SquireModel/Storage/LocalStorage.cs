﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.IO;

namespace Squire.Model.Storage {
	public abstract class LocalStorage {
		protected static string Apppath = Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
			"Squire");
		protected static string Cardspath = Path.Combine(Apppath, "Cards");
		protected static string Graphicspath = Path.Combine(Apppath, "Graphics");
		private readonly WebClientPool _clientPool;
		protected Dictionary<string, Task> LocalFilesWriting = new Dictionary<string, Task>();
		protected object LocalFilesWritingLock = new Object();

		protected LocalStorage(WebClientPool clientPool) {
			_clientPool = clientPool;
		}

		/**
		 * Safety for the same file being downloaded by two tasks
		 */
		protected async Task<bool> StoreAsync(string path, Uri uri) {
			//Console.WriteLine("Request to download " + path);
			Task alreadyWriting = null;
			TaskCompletionSource<bool> tcs = null;
			lock (LocalFilesWritingLock) {
				if (LocalFilesWriting.ContainsKey(path)) {
					alreadyWriting = LocalFilesWriting[path];
				} else {
					tcs = new TaskCompletionSource<bool>();
					LocalFilesWriting[path] = tcs.Task;
					if (File.Exists(path)) {
						Trace.TraceInformation("Already have this: " + path);
						tcs.SetResult(true);
						return false;
					}
				}
			}

			if (alreadyWriting != null) {
				await alreadyWriting;
				return false;
			}

			await _clientPool.Download(uri, path, WebClientPool.ClientPoolPriority.Resource);
			tcs.SetResult(true);

			return true;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Squire.Model.Magic;

using HtmlAgilityPack;

namespace Squire.Model {
	using resourceList = List<Tuple<string, Uri>>;
	public class GathererScraper {
	    readonly WebClientPool _clientPool;
		static readonly Regex GathererIdFromUrl = new Regex("(?<=(multiverseid=))([0-9])*");
		static readonly Regex IntFromSetNumber = new Regex("[0-9]*");

		public GathererScraper(WebClientPool pool) {
			_clientPool = pool;
		}

		public class GetCardReturn {
			private readonly Tuple<MagicCard, resourceList, IEnumerable<int>> _tup;
			public GetCardReturn(MagicCard c, resourceList rl, IEnumerable<int> al) {
				_tup = new Tuple<MagicCard, resourceList, IEnumerable<int>>(c, rl, al);
			}
			public MagicCard Card { get { return _tup.Item1; } }
			public resourceList Resources { get { return _tup.Item2; } }
			public IEnumerable<int> Alternates { get { return _tup.Item3; } }
		}
		public async Task<GetCardReturn> GetCardFromGatherer(int id) {

			var url = new Uri("http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=" + id);

			var doc = await _clientPool.GetDocument(url, WebClientPool.ClientPoolPriority.Getcard);

			var detailsTables = doc.DocumentNode.SelectNodes(
				"//table[@class='cardDetails' or @class='cardDetails cardComponent']");
			var detailsTable = detailsTables.First();

			var divsWithLabelValues = detailsTable.SelectNodes("./tr/td/div/div/div[@class='label']/parent::*");

			var resources = new resourceList();
			var card = new MagicCard();
			card.Faces = new List<MagicFace>();
			card.MultiverseId = id;
			card.CompoundType = MagicCard.ECompoundType.Simple;

			/** multiverse ID of cards with the same name and set (different artwork) **/
			var duplicates = new List<int>();

			/**Get common attributes of all faces by using the Details Table of the first card**/
			foreach (var node in divsWithLabelValues) {
				var labelNodeText = node.SelectSingleNode("./div[@class='label']/text()");
				var labelText = labelNodeText.InnerText.Trim();
				var valueNodeTop = node.SelectSingleNode("./div[@class='value']");
				if (labelText == "Card Number:") {
					var setNumberStr = valueNodeTop.SelectSingleNode("./text()").InnerText.Trim();
					card.SetNumber = Int32.Parse(IntFromSetNumber.Match(setNumberStr).Value);
				} else if (labelText == "Expansion:") {
					var setSymbolName = valueNodeTop.SelectNodes("./div/a/img").Single().Attributes["title"].Value;
					var setSymbolUri = new Uri(
						url,
						WebUtility.HtmlDecode(valueNodeTop.SelectNodes("./div/a/img").Single().Attributes["src"].Value));
					resources.Add(new Tuple<String, Uri>(setSymbolName, setSymbolUri));
					card.MagicSet = valueNodeTop.SelectNodes("./div/a").First(a => a.InnerText.Length > 0).InnerText;
				} else if (labelText == "Rarity:") {
					card.Rarity = valueNodeTop.SelectSingleNode("./span").InnerText.Trim();
				} else if (labelText == "All Sets:") {
					var y = valueNodeTop.SelectNodes(".//a").Where(
						//where the anchor node includes an image which matches current card's set
						anchor => anchor.SelectSingleNode(".//img").
							Attributes.AttributesWithName("title").
							Single().Value.StartsWith(card.MagicSet)
						);
					foreach (var anchorToAlternate in y) {
						var linkToAlternate = anchorToAlternate.Attributes["href"].Value;
						var idOfAlternate = GathererIdFromUrl.Match(linkToAlternate).Value;
						var multiverseId = Int32.Parse(idOfAlternate);
						if (multiverseId != id) {
							duplicates.Add(multiverseId);
						}
					}
				}
			}

			foreach (var faceDetailsTable in detailsTables) {
				var getFaceResponse = GetFace(faceDetailsTable, url);
				getFaceResponse.Item1.Parent = card;
				card.Faces.Add(getFaceResponse.Item1);
				resources.AddRange(getFaceResponse.Item2);
			}

			if (card.Faces.Count > 1) {
				if (card.Faces[0].ImageOnWeb == card.Faces[1].ImageOnWeb) {
					card.CompoundType = MagicCard.ECompoundType.Split;
				} else if (card.Faces[0].ImageOnWeb.AbsoluteUri + "&options=rotate180" == card.Faces[1].ImageOnWeb.AbsoluteUri) {
					card.CompoundType = MagicCard.ECompoundType.Flip;
					//Gatherer incorrectly puts a mana cost on the second face
					card.Faces[1].Cost = "";
				} else {
					card.CompoundType = MagicCard.ECompoundType.Transform;
				}
			}

			return new GetCardReturn(card, resources, duplicates);
		}

		Tuple<string, resourceList> ParseSymbols(Uri context, HtmlNode node, char size) {
			var symbolText = new StringBuilder();
			var resources = new resourceList();
			foreach (var valueAtomNode in node.ChildNodes) {
				if (valueAtomNode.Name == "img") {
					var altAttribute = valueAtomNode.Attributes.First(att => att.Name == "alt");
					var glyphLocation = new Uri(context, WebUtility.HtmlDecode(
						valueAtomNode.Attributes.First(att => att.Name == "src").Value));
					var glyphName = altAttribute.Value;
					resources.Add(new Tuple<string, Uri>(size + glyphName, glyphLocation));
					symbolText.Append("[" + glyphName + "]");
				} else if (valueAtomNode.Name == "#text") {
					symbolText.Append(valueAtomNode.InnerText);
				}
			}
			return new Tuple<string, resourceList>(symbolText.ToString(), resources);
		}

		public async Task<ICollection<int>> GetGathererIdsInSet(MagicSet set) {
			ICollection<int> gathererIds = new HashSet<int>();
			var urlString = "http://gatherer.wizards.com/Pages/Search/Default.aspx?action=advanced&output=checklist&set=[\"" + set.Name + "\"]";
			var document = await _clientPool.GetDocument(urlString, 
				WebClientPool.ClientPoolPriority.Getcard);
			var collection = document.DocumentNode.
				SelectNodes("//tr[@class='cardItem']//td[@class='name']//a[@class='nameLink']");
			foreach (var aNode in collection) {
				var url = aNode.GetAttributeValue("href", "notfound");
				var id = Int32.Parse(GathererIdFromUrl.Match(url).Value);
				gathererIds.Add(id);
			}

			return gathererIds;
		}

		private Tuple<MagicFace, resourceList> GetFace(HtmlNode detailsTable, Uri url) {
			var face = new MagicFace();
			var divsWithLabelValues = detailsTable.SelectNodes("./tr/td/div/div/div[@class='label']/parent::*");

			var resources = new resourceList();

			foreach (var node in divsWithLabelValues) {
				var labelNodeText = node.SelectSingleNode("./div[@class='label']/text()");
				var labelText = labelNodeText.InnerText.Trim();
				if (labelText == "") {
					// P/T label has an additional <b> tag
					var boldNodes = node.SelectNodes("./div[@class='label']/b");
					if (boldNodes!=null && boldNodes.Count > 0) {
						labelText = boldNodes[0].InnerText.Trim();
					}
				}
				var valueNodeTop = node.SelectSingleNode("./div[@class='value']");
				if (labelText == "Card Name:") {
					face.Name = valueNodeTop.SelectSingleNode("./text()").InnerText.Trim();
				} else if (labelText == "Mana Cost:") {
					var parsed = ParseSymbols(url, valueNodeTop, 'l');
					face.Cost = parsed.Item1;
					resources = resources.Union(parsed.Item2).ToList();
				} else if (labelText == "Card Text:") {
					foreach (var valueBlockNode in valueNodeTop.SelectNodes("./div[@class='cardtextbox']")) {
						var parsed = ParseSymbols(url, valueBlockNode, 's');
						face.Text.Add(parsed.Item1);
						resources = resources.Union(parsed.Item2).ToList();
					}
				} else if (labelText == "Flavor Text:") {
					face.Flavor = valueNodeTop.SelectSingleNode("./div[@class='cardtextbox']//text()").InnerText.Trim();
				} else if (labelText == "Types:") {
					var decodeTypesString = valueNodeTop.SelectSingleNode("./text()").InnerText.Trim();
					/** split the Types string. If there is a dash, anything after is a Subtype */
					var typesAndSubtypes = decodeTypesString.Split(new char[] { '—' });
					foreach (var type in typesAndSubtypes[0].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)) {
						if (MagicCard.CardTypes.Contains(type)) {
							face.MagicTypes.Add(type);
						} else /*if (MagicCard.SuperTypes.Contains(type))*/ {
							face.MagicSupertypes.Add(type);
						}
						/*else
						{
							throw new Exception("Didn't recognize card type: " + type);
						}*/

					}
					if (typesAndSubtypes.Length > 1) {
						foreach (var subtype in typesAndSubtypes[1].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)) {
							face.MagicSubtypes.Add(subtype);
						}
					}
				} else if (labelText == "P/T:") {
					var pt = valueNodeTop.SelectSingleNode("./text()").InnerText.Trim();
					var powerIntEx = new Regex("[0-9]+(?=(\\+\\*)?\\s/)");
					var powerStarEx = new Regex("\\*(?=\\s/)");
					var toughIntEx = new Regex("(?<=/\\s)[0-9]+");
					var toughStarEx = new Regex("(?<=/\\s*([0-9]*\\+)?)\\*");
					// p/t can have forms like '* / 1+*'
					if (powerIntEx.IsMatch(pt)) {
						face.Power = Int32.Parse(powerIntEx.Match(pt).Value);
					} else {
						face.Power = 0;
					}
					face.PowerStar = powerStarEx.IsMatch(pt);
					if (toughIntEx.IsMatch(pt)) {
						face.Toughness = Int32.Parse(toughIntEx.Match(pt).Value);
					} else {
						face.Toughness = 0;
					}
					face.ToughnessStar = toughStarEx.IsMatch(pt);
				} else if (labelText == "Loyalty:") {
					face.Loyalty = Int32.Parse(valueNodeTop.
						SelectSingleNode("./text()").InnerText.Trim());
				}
			}

			var imgNode = detailsTable.SelectSingleNode(".//td[@class='leftCol']/img");
			var relativeUrl = imgNode.GetAttributeValue("src", "#");

			face.ImageOnWeb = new Uri(url, WebUtility.HtmlDecode(relativeUrl));
			return new Tuple<MagicFace, resourceList>(face, resources);
		}
	}
}
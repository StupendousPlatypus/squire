﻿using System;
using Squire.Model.Magic;

namespace Squire.Model.Filters {
	public class NameFilter : ISquireFilter {
		public String SearchedString { get; set; }
		public NameFilter() {
			SearchedString = "";
		}
		public bool FilterImpl(object cardObj) {
			if (SearchedString == "") {
				return true;
			}
			var card = cardObj as MagicCard;
			if (card== null) {
				return false;
			}
			return card.Name.ToUpper().Contains(SearchedString.ToUpper());
		}
	}
}
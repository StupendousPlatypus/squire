﻿using Squire.Model.Magic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Squire.Model.Filters {
	public class ColorFilter : ISquireFilter {
		public class ColorChoice {
			public EventHandler SelectionChangedEvent = delegate { };
			public string Name { get; set; }
			bool _selected;
			public bool IsSelected {
				get { return _selected; }
				set {
					_selected = value;
					SelectionChangedEvent(this, EventArgs.Empty);
				}
			}
		}
		public ICollection<ColorChoice> ColorChoices { get; set; }
		public ColorFilter() {
			ColorChoices = new List<ColorChoice>();
			foreach (var color in new string[]{"Black", "Red", "Green", "White", "Blue"}){
				ColorChoices.Add(new ColorChoice { Name = color, IsSelected = false });
			}
		}
		public bool FilterImpl(object cardObj) {
			var card = cardObj as MagicCard;
			if (card == null) {
				return false;
			}
			if (ColorChoices.All(c => !c.IsSelected)) {
				return true;
			}
			
			var selectedColors = from c in ColorChoices
								   where c.IsSelected
								   select c.Name;
			foreach (var s in selectedColors) {
				if (card.Cost == null) {
					return false;
				}
				if (!card.Cost.Contains(s)) {
					return false;
				}
			}
			return true;
		}
	}
}

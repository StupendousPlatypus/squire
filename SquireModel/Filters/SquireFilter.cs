﻿namespace Squire.Model.Filters {
	public interface ISquireFilter {
		bool FilterImpl(object cardObj);
	}
}
﻿using Squire.Model.Magic;
using System.Collections.ObjectModel;

using Squire.Model.Storage;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Squire.Model {
	public class SquireModel {
	    readonly WebClientPool _webClientPool = new WebClientPool(4);
		public FaceImageStorage ImageStorage;
		public GlyphStorage GlyphStorage;
		private SerializedCardStorage CardStorage;
		public ObservableCollection<MagicCard> CardList = new ObservableCollection<MagicCard>();
		public ExpansionsDownloader Downloader {get;set;}
		public SquireModel() {
			ImageStorage = new FaceImageStorage(_webClientPool);
			GlyphStorage = new GlyphStorage(_webClientPool);
			CardStorage = new SerializedCardStorage(_webClientPool);
			Downloader = new ExpansionsDownloader(GlyphStorage, CardStorage, _webClientPool, CardList);
		}
		public async Task LoadCards(){
			var sets = (new MagicSetLoader()).LoadSetNames();
			await Downloader.GetFromLocal(sets);
			await Downloader.GetFromGatherer(sets);
			return;
		}
	}
}

﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Squire.Model.Magic;
using System.Threading.Tasks;
using System.ComponentModel;

using Squire.Model.Storage;

namespace Squire.Model {
	public class ExpansionsDownloader {
		public event PropertyChangedEventHandler PropertyChanged = delegate { };
		private readonly GlyphStorage _glyphStorage;
		private readonly SerializedCardStorage _cardStorage;
	    private readonly MagicSetLoader _setLoader = new MagicSetLoader();
	    readonly Collection<MagicCard> _listCards;
		private readonly WebClientPool _pool;
		private String _currentTask;
	    private bool _gathererEverUsed = false;
        public String CurrentTask
        {
            get { return _currentTask; }
            set
            {
                _currentTask = value;
				PropertyChanged(this, new PropertyChangedEventArgs("CurrentTask"));
            }
		}
		public int CardsInSet { get; set; }
        private int _cardInSetIndex;
        public int CardInSetIndex
        {
            get { return _cardInSetIndex; }
            set
            {
                _cardInSetIndex = value;
				PropertyChanged(this, new PropertyChangedEventArgs("PercentProgress"));
            }
        }

		private bool _active;
		public bool IsActive {
			get { return _active; }
            set
            {
                _active = value;
				PropertyChanged(this, new PropertyChangedEventArgs("IsActive"));
            }
		}
		public double PercentProgress {
			get {
				if (CardsInSet <= 0) {
					return 1;
				}
				return (double) CardInSetIndex * 100 / CardsInSet;
			}
		}
		public ExpansionsDownloader(GlyphStorage glyphStorage,
            SerializedCardStorage cardStorage,
            WebClientPool pool,
            Collection<MagicCard> listCards) {
            _glyphStorage = glyphStorage;
            _cardStorage = cardStorage;
            _pool = pool;
            _active = false;
            _listCards = listCards;
            CurrentTask = "";
        }

	    public async Task GetFromLocal(IEnumerable<MagicSet> sets ) {
	        Debug.Assert(!_gathererEverUsed);
            IsActive = true;
            CurrentTask = "Loading from filesystem";
            await _cardStorage.GetCardsFromFileSystem(sets, _listCards);
	        IsActive = false;
	    }

	    public async Task GetFromGatherer(IEnumerable<MagicSet> sets) {
	        _gathererEverUsed = true; //for assertion that GetFromLocal never called after GetFromGatherer
            var scraper = new GathererScraper(_pool);  
            IsActive = true;
            foreach (var set in sets) {
                if (_cardStorage.IsSetComplete(set)) {
                    continue;
                }

                var idsInSet = await scraper.GetGathererIdsInSet(set);
                var idsInFileSystem = new HashSet<int>(from card in _listCards
                                                       where (card.MagicSet == set.Name)
                                                       select card.MultiverseId);
                CurrentTask = set.Name;
                CardsInSet = idsInSet.Count;
                CardInSetIndex = 0;

                var idsToGet = new List<int>();
                foreach (var idInSet in idsInSet.Distinct()) {
                    if (idsInFileSystem.Contains(idInSet)) {
                        CardInSetIndex++;
                    } else {
                        idsToGet.Add(idInSet);
                    }
                }

                var tasksForSet = idsToGet.Select(i => GetCardAsync(scraper, i)).ToList();
                await Task.WhenAll(tasksForSet);

                _cardStorage.NoteSetComplete(set);
            }
            IsActive = false;
	    }
		public async Task GetCardAsync(GathererScraper scraper, int multiverseId) {
			var getCardResponse = await scraper.GetCardFromGatherer(multiverseId);
			_cardStorage.StoreCard(getCardResponse.Card);
			foreach (var resource in getCardResponse.Resources) {
				await _glyphStorage.StoreGlyphAsync(resource.Item1, resource.Item2);
			}

			CardInSetIndex++;

			_listCards.Add(getCardResponse.Card);
			foreach (var alternateId in getCardResponse.Alternates) {
				/* workaround for a bug in Gatherer. For a card with multiple versions in
				 * one set, Gatherer will give us duplicates of a single version's ID*/
				var altGetCardResponse = await scraper.GetCardFromGatherer(alternateId);
				_cardStorage.StoreCard(altGetCardResponse.Card);
				foreach (var resource in altGetCardResponse.Resources) {
					await _glyphStorage.StoreGlyphAsync(resource.Item1, resource.Item2);
				}
				_listCards.Add(altGetCardResponse.Card);

				CardInSetIndex++;

			}
		}
	}
}

﻿using System.Linq;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Squire.Model {
	/**
	 * Ensures a limit on how many http requests are sent at once, and
	 * prioritizes pending requests
	 */
	public class WebClientPool {

		public enum ClientPoolPriority {
			Resource,
			Face,
			/**happens in the background and lowest priority*/
			Getcard
		}
		private int _numClientsAvailable;
		/**
		 * Store requests from tasks which need to use a WebClient.
		 * They're sorted by priority
		 */
		private readonly Dictionary<ClientPoolPriority, Queue<SemaphoreSlim>> _prioritizedQueues;
		/**
		 * Lock on both numClientsAvailable and prioritizedQueues
		 */
		private readonly object _lock = new Object();

		public WebClientPool(int numClients) {
			_numClientsAvailable = numClients;

			/**construction of the priority queues**/
			_prioritizedQueues = new Dictionary<ClientPoolPriority, Queue<SemaphoreSlim>>();
			foreach (ClientPoolPriority p in Enum.GetValues(typeof(ClientPoolPriority))) {
				_prioritizedQueues.Add(p, new Queue<SemaphoreSlim>());
			}
		}

		public async Task<HtmlDocument> GetDocument(Uri uri, ClientPoolPriority priority) {
			await EnterSemaphore(priority);
			var client = InitWebClient();
			var htmlString = await client.DownloadStringTaskAsync(uri);
			var document = new HtmlDocument();
			document.LoadHtml(htmlString);
			ExitSemaphore();
			return document;
		}

		public async Task<HtmlDocument> GetDocument(String uri, ClientPoolPriority priority) {
			return await GetDocument(new Uri(uri), priority);
		}

		public async Task Download(Uri uri, string localPath, ClientPoolPriority priority) {
			await EnterSemaphore(priority);
			var client = InitWebClient();
			await client.DownloadFileTaskAsync(uri, localPath);
			ExitSemaphore();
			return;
		}

		private Queue<SemaphoreSlim> HighestNonemptyQueue()
		{
		    return (from ClientPoolPriority c in Enum.GetValues(typeof (ClientPoolPriority)) 
                   where _prioritizedQueues[c].Count > 0 
                   select _prioritizedQueues[c]).FirstOrDefault();
		}

	    private async Task EnterSemaphore(ClientPoolPriority priority) {
			SemaphoreSlim request = null;
			lock (_lock) {
				if (_numClientsAvailable > 0) {
					_numClientsAvailable--;
				} else {
					request = new SemaphoreSlim(0);
					_prioritizedQueues[priority].Enqueue(request);
				}
			}
			if (request != null) {
				await request.WaitAsync();
			}
			return;
		}
		private void ExitSemaphore() {
			//release the highest priority semaphore, or increment numClientsAvailable
			SemaphoreSlim nextRequest = null;
			lock (_lock) {
				var selectedQueue = HighestNonemptyQueue();
				if (selectedQueue != null) {
					nextRequest = selectedQueue.Dequeue();
				}
				if (nextRequest == null) {
					_numClientsAvailable++;
				}
			}
			if (nextRequest != null) {
				nextRequest.Release(1);
			}
		}

		private WebClient InitWebClient() {
			var newClient = new WebClient();
			newClient.Headers.Add("User-Agent", "Gatherer scraper");
			newClient.Encoding = UTF8Encoding.UTF8;
			return newClient;
		}
	}
}
